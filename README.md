# README

## Compilation:
### ajouter les paths de systemc-2.3.3 et de la toolchain risc au fichier setup-ensimag.sh 
### Liens utiles:
risc-v toolchain download:  https://github.com/stnolting/riscv-gcc-prebuilt/releases/download/rv64imc-3.0.0/riscv64-unknown-elf.gcc-12.1.0.tar.gz

systemC download: https://accellera.org/downloads/standards/systemc

##  Execution:
Pour chaque cas (native ou ISS) lancer le fichier run correspondant.

## Resultats:
<p style="text-align: center;">
<img src="img/capture.png" alt="drawing" width="400"  />
</p>




